package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class BoiledClamsTest {
    private Clams clams;

    @Before
    public void setUp() {
        clams = new BoiledClams();
    }

    @Test
    public void testBoiledClamsToStringMethod() {
        assertEquals(clams.toString(), "Boiled Clams from Ciliwung");
    }
}
