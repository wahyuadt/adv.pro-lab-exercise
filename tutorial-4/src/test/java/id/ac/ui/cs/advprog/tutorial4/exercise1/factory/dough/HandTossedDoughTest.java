package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class HandTossedDoughTest {
    private Dough dough;

    @Before
    public void setUp() {
        dough = new HandTossedDough();
    }

    @Test
    public void testHandTossedDoughToStringMethod() {
        assertEquals(dough.toString(), "Normal handmade dough");
    }
}
