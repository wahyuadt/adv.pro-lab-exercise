package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class RawSauceTest {
    private Sauce sauce;

    @Before
    public void setUp() {
        sauce = new RawSauce();
    }

    @Test
    public void testRawSauceToStringMethod() {
        assertEquals(sauce.toString(), "No Ketchup, Just Sauce, Raw Sauce");
    }
}
