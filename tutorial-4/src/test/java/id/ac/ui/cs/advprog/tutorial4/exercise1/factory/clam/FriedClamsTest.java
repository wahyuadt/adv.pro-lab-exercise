package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FriedClamsTest {
    private Clams clams;

    @Before
    public void setUp() {
        clams = new FriedClams();
    }

    @Test
    public void testFriedClamsToStringMethod() {
        assertEquals(clams.toString(), "Fried Clams from Bahari Jaya");
    }
}
