package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class HandTossedDough implements Dough {
    public String toString() {
        return "Normal handmade dough";
    }
}
