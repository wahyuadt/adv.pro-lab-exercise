package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    private static volatile Singleton uniqueInstance;

    private Singleton() {}
    // TODO Implement me!
    // What's missing in this Singleton declaration?

    public static Singleton getInstance() {
        if (uniqueInstance == null) {
            synchronized (Singleton.class) {
                if (uniqueInstance == null) {
                    uniqueInstance = new Singleton();
                }
            }
        }
        // TODO Implement me!
        return uniqueInstance;
    }
}
