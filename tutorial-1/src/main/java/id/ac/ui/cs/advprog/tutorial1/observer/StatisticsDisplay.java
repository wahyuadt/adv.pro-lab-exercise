package id.ac.ui.cs.advprog.tutorial1.observer;

import java.util.Observable;
import java.util.Observer;

public class StatisticsDisplay implements Observer, DisplayElement {

    private Observable observable;
    private float maxTemp = 0.0f;
    private float minTemp = 200;
    private float tempSum = 0.0f;
    private int numReadings;

    public StatisticsDisplay(Observable observable) {
        this.observable = observable;
        observable.addObserver(this);
        // TODO Complete me!

    }

    @Override
    public void display() {
        System.out.println("Avg/Max/Min temperature = " + (tempSum / numReadings)
                + "/" + maxTemp + "/" + minTemp);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            WeatherData weatherData = (WeatherData)o;
            if (weatherData.getTemperature() < minTemp) {
                minTemp = weatherData.getTemperature();
            } else if (weatherData.getTemperature() > maxTemp) {
                maxTemp = weatherData.getTemperature();
            }

            this.tempSum += weatherData.getTemperature();
            ++this.numReadings;
            display();

            // TODO Complete me!
        }
    }
}
