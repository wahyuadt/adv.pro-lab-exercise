package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;

public class Main {

    public static void main(String[] args) {
        Company fightInteractive = new Company();
        fightInteractive.addEmployee(new FrontendProgrammer("Wahyu",40000));
        fightInteractive.addEmployee(new SecurityExpert("Badim", 70001));
        fightInteractive.addEmployee(new Ceo("Danny",400000));
        System.out.println(fightInteractive.getNetSalaries());
        for (Employees anggota : fightInteractive.getAllEmployees()) {
            System.out.println(anggota.name + " Work as " + anggota.role
                                + " and paid with salary " + anggota.salary);
        }
    }
}
