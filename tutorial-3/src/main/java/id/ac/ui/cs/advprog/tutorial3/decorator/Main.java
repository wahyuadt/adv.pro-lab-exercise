package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChickenMeat;

public class Main {
    public static void main(String[] args) {
        Food burger = new ThickBunBurger();
        burger = new Cheese(burger);
        burger = new ChickenMeat(burger);
        System.out.printf("%s $%.2f", burger.getDescription(), burger.cost());
    }
}
